select
  count(distinct session_id),
  CASE
    WHEN user_agent iLIKE '%ewRelicPinger%'
      or user_agent iLIKE '%check_http%'
      or user_agent iLIKE '%Seoradar%'
      or user_agent iLIKE '%Http_client%'
      or user_agent iLIKE '%pingbot%'
      or user_agent iLIKE '%Pingdom%'
    THEN 'self-test'
    WHEN user_agent iLIKE '%bot%'
      or user_agent iLIKE '%crawl%'
      or user_agent iLIKE '%pider%'
      or user_agent iLIKE '%scrape%'
    THEN 'bot'
    ELSE 'user'
  END as purpose,
  CASE
    WHEN user_agent iLIKE '%iPad%' THEN 'iPad'
    WHEN user_agent iLIKE '%iPhone%' THEN 'iPhone'
    WHEN user_agent iLIKE '%SAMSUNG%' THEN 'Samsung'
    WHEN user_agent iLIKE '%BlackBerry%' THEN 'BlackBerry'
    ELSE 'unknown'
  END as device,
  CASE
    WHEN user_agent iLIKE '%OS X%' THEN 'OS X'
    WHEN user_agent iLIKE '%Windows%' THEN 'Windows'
    WHEN user_agent iLIKE '%Android%' THEN 'Android'
    WHEN user_agent iLIKE '%Linux%' or user_agent LIKE '%X11%' THEN 'Linux'
    WHEN user_agent iLIKE '%SMART-TV%' THEN 'Smart TV'
    ELSE 'unknown'
  END as os,
  CASE
-- browsers  
    WHEN user_agent iLIKE '%Chrome/%' OR user_agent LIKE '%CriOS%' THEN 'Chrome'
    WHEN user_agent iLIKE '%Firefox/%' THEN 'Firefox'
    WHEN user_agent iLIKE '%MSIE%' THEN 'IE'
    WHEN user_agent iLIKE '%Trident%' THEN 'IE'
    WHEN user_agent iLIKE '%Opera%' THEN 'Opera'
    WHEN user_agent iLIKE '%Android%' THEN 'Android'
    WHEN user_agent iLIKE '%Safari%' THEN 'Safari'
    WHEN user_agent iLIKE '%Mozilla%' THEN 'Mozilla'
    ELSE 'unknown'
  END as browser,
  CASE
-- social
    WHEN user_agent iLIKE '%facebookexternalhit%' THEN 'FaceBook'
    WHEN user_agent iLIKE '%WordPress%' THEN 'WordPress'
-- bots
    WHEN user_agent iLIKE '%ewRelicPinger%' THEN 'NewRelicPinger'
    WHEN user_agent iLIKE '%bot%' THEN 'Bot'
    WHEN user_agent iLIKE '%crawl%' or user_agent LIKE '%spider%' or user_agent LIKE '%scrape%' THEN 'Bot'
--    WHEN user_agent LIKE '%http://%' THEN 'Bot'
--    WHEN user_agent LIKE '%www.%' THEN 'Bot'
--    WHEN user_agent LIKE '%Wget%' THEN 'Bot'
--    WHEN user_agent LIKE '%curl%' THEN 'Bot'
--    WHEN user_agent LIKE '%urllib%' THEN 'Bot'
-- home grown
    WHEN user_agent iLIKE '%BizTalk%' THEN 'BizTalk'
    WHEN user_agent iLIKE '%python%' THEN 'python'
    WHEN user_agent iLIKE '%Java%' THEN 'java'
    WHEN user_agent iLIKE '%Guzzle%' THEN 'guzzle'
    WHEN user_agent iLIKE '%Go%' THEN 'go'
    WHEN user_agent iLIKE '%Ruby%' THEN 'ruby'
    WHEN user_agent iLIKE '%curl%'
      or user_agent iLIKE '%wget%'
--     or user_agent iLIKE '%HTTP%'
--     or user_agent iLIKE '%http%'
    THEN 'cli'
    WHEN user_agent iLIKE '%Microsoft Office%'
      or user_agent iLIKE '%WebDAV%'
      or user_agent iLIKE '%Microsoft Windows%'
    THEN 'MS Office'
--    ELSE user_agent
--    ELSE 'junk'
  END as parser
from lamp_log_http_request where request_time > '2016-04-21'
group by 2,3,4,5,6
order by count(distinct session_id) desc, 2,3,4;
