import requests

# These are our demo API keys, you can use them!
api_key = 'simplyrets'
api_secret = 'simplyrets'
api_url = 'https://api.simplyrets.com/properties'

response = requests.get(api_url, auth=(api_key, api_secret))
res = (response.json())
