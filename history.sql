select * from working.vw_opportunity limit 10;

select 
  vintage,
  count(*) as loans,
  stage,
  med_apr,
  med_interest
from (
  select
    vintage,
    stage,
    floor(median((apr)) over (partition by vintage))::decimal as med_apr,
    floor(median((interest)) over (partition by vintage))::decimal as med_interest
  from (
    select
      to_char(date_trunc('year', date_funded),'yyyy') as vintage,
      stage,
      apr,
      interest
    from working.vw_opportunity
    where stage = 'Funded'
    ) sub1
  ) sub2
group by 
  vintage,
  stage,
  med_apr,
  med_interest
order by 1;



select
      to_char(date_trunc('month', date_funded),'yyyy-mm') as mo_funded,
      (case when date_step1 = date_step5 then 0 else (select count(*) - 1 from working.user_date_series where short_date between date_step1 and date_step5 and is_weekend = False) end) as bstep1vs5,
      (case when date_step1 = date_funded then 0 else (select count(*) - 1 from working.user_date_series where short_date between date_step1 and date_funded and is_weekend = False) end) as bstep1vsfund,
      (case when date_step5 = date_funded then 0 else (select count(*) - 1 from working.user_date_series where short_date between date_step5 and date_funded and is_weekend = False) end) as bstep5vsfund
    from working.vw_opportunity_source
    where
      date_funded >= '2015-08-01'
      and id_loan > ''
