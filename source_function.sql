--create table looker_scratch.test_table(id bigint, name varchar(25));
--select * from looker_scratch.test_table;

/*
CREATE [ OR REPLACE ] FUNCTION f_function_name 
( [ argument_name arg_type [ , ... ] ] )
RETURNS data_type
{ VOLATILE | STABLE | IMMUTABLE }  
AS $$
  python_program
$$ LANGUAGE plpythonu;
*/


CREATE OR REPLACE FUNCTION udf_get_source_from_utm(utm_source varchar(255), utm_campaign varchar(255)) 
RETURNS varchar(255)
IMMUTABLE
AS $$
  result = ''
  if utm_source[:2] == 'DM':                             result = 'Direct Mail'
  elif utm_source == 'Turing':                           result = 'Sprinklr'
  elif utm_source == '':                                 result = 'Organic'
  elif utm_source == 'google' and 'SEM' in utm_campaign: result = 'SEM-google' 
  elif utm_source == 'bing' and 'SEM' in utm_campaign:   result = 'SEM-bing' 
  elif utm_source == 'yahoo' and 'SEM' in utm_campaign:  result = 'SEM-yahoo'
  else:                                                  result = utm_source
  return result 
$$ LANGUAGE plpythonu;


/*

CREATE OR REPLACE FUNCTION udf_get_source_v2(utm_source varchar(255), utm_campaign varchar(255)) 
RETURNS varchar(255)
IMMUTABLE
AS $$
  result = ''
  if utm_source[:2] == 'DM':                             result = 'Direct Mail'
  elif utm_source == 'Turing':                           result = 'Sprinklr'
  elif utm_source == '':                                 result = 'Organic'
  elif utm_source == 'google' and 'SEM' in utm_campaign: result = 'SEM-google' 
  elif utm_source == 'bing' and 'SEM' in utm_campaign:   result = 'SEM-bing' 
  elif utm_source == 'yahoo' and 'SEM' in utm_campaign:  result = 'SEM-yahoo'
  else:                                                  result = utm_source
  return result 
$$ LANGUAGE plpythonu;

*/
