#!usr/bin/env python

# == load libraries ==

# key libraries
import pandas as pd
import numpy as np
import csv                                      # for csv sniffer
import logging
import requests                                 # requesting URL's
import html5lib

## === 3rd party ===
try    : import simplejson as json
except : import json
import yaml

# == set logging ==
logger = logging.getLogger(__name__)
#logger = logging.getLogger(__name__).addHandler(logging.NullHandler())

# == data ==

def getHTML(url):
    '''use pandas to capture html table data'''
    response = requests.get(url)
    return response.content

def HTML2df(html):      # requires html5lib
    '''load html table data to pandas DataFrame'''
    return pd.read_html(html)

def mergeDFs(df1, df2, df1_cols, df2_cols, join_type='outer'):
    '''merge df1 and df2 on df1_columns (list) and df2_columns (list) using join type'''
    df3 = pd.merge(df1, df2, left_on=df1_cols, right_on=df2_cols, how=join_type)
    return df3

def crosstabDF(df):
    ''' create cross tab of two attributes, remove zeros for readability '''
    return(pd.crosstab(tx.Offer, tx.Cust_ln).apply(lambda x: x, axis=1).replace(0,''))

def obj2float(df, columns):
    '''convert dataframe columns from object to float'''
    for column in columns:
        df.column = df.column.str.replace(',','').astype(float)

def isolateMissing():
    '''isolate columns that aren't mapping'''
    return df3[df3.isnull().any(axis=1)][['name','company']]

# == helper functions ==

def strFormat(text, color):
    key = {
        'head'  : "\x1b[",
        'end'   : "\x1b[0m",
        'red'   : "0;30;41m",
        'green' : "0;30;42m",
        'orange': "0;30;43m",
        'blue'  : "0;30;44m",
        'purple': "0;30;45m",
        'gold'  : "0;30;46m",
        'white' : "0;30;47m"
    }
    return (key['head'] + key[color] + text + key['end'])

def setDF(w=None, c=None, r=None, f='{:,.1f}'):
    '''set width, max columns and rows'''
    pd.set_option('display.width', w)               # show columns without wrapping
    pd.set_option('display.max_columns', c)         # show all columns without elipses (...)
    pd.set_option('display.max_rows', r)            # show default number of rows for summary
    pd.options.display.float_format = f.format
    np.set_printoptions(formatter={'float': lambda x: f.format(x)})
def summarizeData(desc, data):
    '''summarize dataframe data, separate data summary/reporting'''     # test if this works for data set that is not a dict
    logger.info("\n\n" + strFormat(" {} ".format(desc),'green') +
        strFormat(" dataset has {} records with {} features".format(*data.shape),'white') + "\n")
    for index, item in enumerate(sorted(data.columns)): logger.info("\t{}\t'{}'".format(index + 1, item))
    logger.info("\n\n== DataFrame Description ==\n\n" + str(data.describe(include='all')) + "\n\n")
    # logger.info(data.describe(include='all'))
    logger.info("\n\n== DataFrame, head ==\n\n" + str(data.head()) + "\n\n")


def dfCol2Numeric(df, cols):
    '''use pandas apply to convert columns to numeric type'''
    # cols = df.columns.drop('id')
    # df[cols] = df[cols].apply(pd.to_numeric, errors='coerce')
    return df[cols].apply(pd.to_numeric, errors='coerce')

# == i/o ==

def sniffDelim(file):
    '''helper functionuse csv library to sniff delimiters'''
    with open(file, 'r') as infile:
        dialect = csv.Sniffer().sniff(infile.read())
    return dict(dialect.__dict__)

def readFile(file):
    '''simple read file'''
    with open(file, 'r') as infile: data=infile.read()
    return data

def writeCSV(file, data, headers=None):
    ''' saves data to csv delimited file '''
    with open(file, 'w') as outfile:
        csv_writer = csv.writer(outfile, dialect='excel')
        csv_writer.writerows(data)

def yaml2file(file,dict):
    with open(file, 'w') as fh:
        outfile.write(yaml.dump(data, default_flow_style=False))
    py_wc(file)
    return

def loadData(file):
    '''check file delimiter and load data set as pandas.DataFrame'''
    try:
        logger.debug('\n\tchecking delimiter')
        delimiter = sniffDelim(file)['delimiter']
        logger.debug('\tdelimiter character identified: {}'.format(delimiter))
        try:
            data = pd.read_csv(file, sep=delimiter)
            logger.debug("\tfile loaded")
            return data
        except UnicodeDecodeError:
            logger.error('\tunicode error, trying latin1')
            data           = pd.read_csv(file, encoding='latin1', sep=delimiter) # including sep in this call fails...
            logger.debug("\tfile loaded")
            return data
    except Exception as e:
        raise e
