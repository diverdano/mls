#!/usr/bin/env python

## ==================================
## === packages, modules, pragmas ===
## ==================================

## === built-ins ===
#import sys
from datetime   import date, timedelta, datetime
from time       import localtime, strftime, strptime, sleep, mktime
import logging
import logging.config                                       # pythons logging feature
import subprocess
import smtplib
import requests

## ===================================
## === logging to file and console ===
## ===================================

def setLogLevel(debug_level, log_file):
    ''' set logging debug level '''
    ERROR_FORMAT        = "%(asctime)s %(name)s %(levelname)-8s %(message)s"
    INFO_FORMAT         = "%(asctime)s %(name)s %(levelname)-8s %(message)s"
    CONSOLE_FORMAT      = "\t%(message)s"
    if debug_level == True:
        DEBUG_FORMAT    = "%(asctime)s %(name)s %(levelname)-8s %(filename)s->%(funcName)s line %(lineno)d: %(message)s"
        LOG_LEVEL       = "DEBUG"
    else:
        DEBUG_FORMAT    = INFO_FORMAT
        LOG_LEVEL       = "INFO"
    LOG_CONFIG = {'version':1,
                  'formatters':{'error':{'format':ERROR_FORMAT},
                                'info':{'format':INFO_FORMAT},
                                'console':{'format':CONSOLE_FORMAT},
                                'debug':{'format':DEBUG_FORMAT}},
                  'handlers':{'console':{'class':'logging.StreamHandler',
                                         'formatter':'console',
                                         'level':logging.DEBUG},
                              'file':{'class':'logging.FileHandler',
                                      'filename':log_file,
                                      'formatter':'debug',
                                      'level':logging.INFO}},
                  'root':{'handlers':['console', 'file'], 'level':LOG_LEVEL}}
    logging.config.dictConfig(LOG_CONFIG)

# == set logging ==
logger = logging.getLogger(__name__)

## =========================
## === support functions ===
## =========================

def date2epoch(value, option=1):
    if int(option) == 1  : pattern = '%Y-%m-%d'
    elif int(option) == 2: pattern = '%Y-%m'
    elif int(option) == 3: pattern = '%Y-%m-%dT%H:%M:%SZ'
    elif int(option) == 4: pattern = '%Y-%m-%d %H:%M:%S'
    elif int(option) == 5: pattern = '%m/%d/%Y'
    return int(mktime(strptime(value, pattern)))

def epoch2date(value, option=1):
    if int(option) == 1  : pattern = '%Y-%m-%d'
    elif int(option) == 2: pattern = '%Y-%m'
    elif int(option) == 3: pattern = '%Y-%m-%dT%H:%M:%SZ'
    elif int(option) == 4: pattern = '%Y-%m-%d %H:%M:%S'
    elif int(option) == 5: pattern = '%m/%d/%Y'
    return (strftime(pattern, localtime(value)))

def displayTime(value=None, offset=0, option=1, strip_option=1):
    ''' date passed as string '''
    offset = int(offset)
    if value == None: d = (date.today() + timedelta(offset))
    else:
        start_date  = date2epoch(value, option=strip_option)
        end_date    = start_date + offset * 86400
        d           = localtime(end_date)
    if   option == 1: return strftime('%Y-%m-%d', d)
    elif option == 2: return strftime('%Y-%m-%d %H:%M:%S', d)
    elif option == 3: return strftime('%H:%M:%S', d)
    elif option == 4: return strftime('%Y-%m-%d %H:%M', d)
    elif option == 5: return strftime('%Y%m%d', d)
    elif option == 6: return strftime('%Y%m%dT%H:%M', d)
    elif option == 7: return strftime('%Y-%m-%dT%H:%M', d)
    else            : return strftime('%s', d)

def pickDate(offset=0, show_date=True):
    offset = int(offset)
    now = datetime.now()+timedelta(offset)
    day = datetime(now.year, now.month, now.day)
    if show_date == True: logger.info('datestamp applied: ' + day.strftime("%Y-%m-%d"))
    return day.strftime("%Y-%m-%d")

def titleExcept(s, exceptions=[]):
    word_list = s.split()
    final = [word_list[0].capitalize()]
    for word in word_list[1:]:
        final.append(word.lower() if word.lower() in exceptions else word.capitalize())
    return " ".join(final)

## =====================
## === i/o functions ===
## =====================

def ssh2shell(location='cltqtflpclt08', command='ls', function='Popen'):
    print(logger('IO shell') + location + ' ' + command)
    if      function == 'call'          : return subprocess.call(["ssh", location, command])
    elif    function == 'check_call'    : return subprocess.check_call([sub_command])
    elif    function == 'check_output'  : return subprocess.check_output([sub_command])
    else                                :
        ssh = subprocess.Popen(
            ["ssh", location, command],
            shell=False,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        result  = ssh.stdout.readlines()
        error   = ssh.stderr.readlines()
        if result == []:
##            print(logger('stderr') + str('\t'.join([item for item in sys.stderr.readlines()])))
            print(logger('IO ERROR') + '\n\t' + str('\t'.join([item for item in error])))
            return (sys.stderr, "ERROR: %s" % error)
        else:
            return result
#            print(result)

def email(FROM, TO, SUBJECT, TEXT, SERVER='localhost'):
    if      type(TO) is list    : to = ", ".join(TO)
    elif    type(TO) is str     : to = TO
    message = """From: %s\nTo: %s\nSubject: %s\n\n%s""" % (FROM, to, SUBJECT, TEXT)
    print(logger('IO smtp'))
    print('\tfrom:    ' + FROM)
    print('\tto:      ' + to)
    print('\tsubject: ' + SUBJECT)
    print('\tmessage:\n\t\t' + str('\n\t\t'.join([item for item in TEXT.split('\n')])))
    server = smtplib.SMTP(SERVER)
    server.sendmail(FROM, TO, message)
    server.quit()

def getAPIResponse(api_url, api_key, api_secret):
    '''simple api request/response'''
    return requests.get(api_url, auth=(api_key, api_secret))
