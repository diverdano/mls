/*

select * from cbl_landing.payment_reg limit 10;
select * from cbl_landing.svc_fees limit 10;
select * from cbl_landing.svc_loans limit 10;
select * from cbl_landing.sfrpt_inv_transactions limit 10;
select count(*) from cbl_landing.payment_reg;
select count(*) from cbl_landing.svc_fees;
select count(*) from cbl_landing.svc_loans;
select count(*) from cbl_landing.sfrpt_inv_transactions;

*/


select type, count(*) from cbl_landing.sfrpt_inv_transactions group by 1 order by 1;
where type = 'Service Fee on Interest'
;
select
--  t.note_name,
  to_char(cast(t.transaction_date as date), 'YYYY-MM'),
--  t.type,
--  l.loan_status,
--  l.term,
--  l.cbl_grade,
  sum(t.interest) as interest,
  sum(t.principal)as principal,
--  sum(t.principal_remaining) as balance,
  sum(t.service_fees) as svc_fee,
--  sum(t.amount) as amount,
--  sum(t.net_received) as net,
--  l.cl_contract_id,
--  l.funding_date,
--  l.current_interest_rate,
  sum(l.original_value) as orig_value
from cbl_landing.sfrpt_inv_transactions t
left join cbl_landing.svc_loans l
  on t.note_name = l.note_name
where type in ('Note Interest Payment','Note Principal Payment','Service Fee on Interest Payment')
group by 1--,2--,3,4
order by 1--,2--,3,4
--limit 10
;

select
  funding_date as date_funded,
  cbl_grade as grade,
  loan_status as status,
  principal_remaining as remaining_balance,
  term,
  current_interest_rate as rate,
  *
from cbl_landing.svc_loans limit 10;

select
  to_char(cast(date_funded as date), 'YYYY-MM') as vintage,
--  date_funded,
--  stage,
  status,
  grade,
  term,
  fico,
  interest,
  principal
--  , *
from public.vw_opportunity_source
where stage = 'Funded'
limit 10;
