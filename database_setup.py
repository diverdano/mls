import sys
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine

Base = declarative_base()

class Restaurant(Base):
    __tablename__   = 'restaurant'
    id              = Column(Integer, primary_key = True)
    name            = Column(String(80), nullable = False)
    def __repr__(self):
        return("\n\nrestaurant id: " + "%s" +
                "\n name".ljust(14) + "%s") % (self. id, self.name)

class MenuItem(Base):
    __tablename__   = 'menu_item'
    id              = Column(Integer, primary_key = True)
    restaurant_id   = Column(Integer, ForeignKey('restaurant.id'))
    restaurant      = relationship(Restaurant)
    name            = Column(String(80), nullable = False)
    course          = Column(String(250))
    desc            = Column(String(250))
    price           = Column(String(8))
    def __repr__(self):
        return("\n\nmenu item id: " + "%s" +
                "\n restaurant".ljust(14) + "%s" +
                "\n course".ljust(14) + "%s" +
                "\n price".ljust(14) + "%s" +
                "\n name".ljust(14) + "%s" +
                "\n desc".ljust(14) + "%s") % (self.id, self.restaurant.name, self.course, self.price, self.name, self.desc)

class Employee(Base):
    __tablename__ = 'employee'
    id              = Column(Integer, primary_key = True)
    name            = Column(String(250), nullable = False)
    def __repr__(self):
        return("\n\nemployee id: " + "%s" +
                "\n name".ljust(14) + "%s") % (self. id, self.name)

class Address(Base):
    __tablename__ = 'address'
    id              = Column(Integer, primary_key = True)
    employee_id     = Column(Integer, ForeignKey('employee.id'))
    employee        = relationship(Employee)
    street          = Column(String(80), nullable = False)
    zip             = Column(String(5), nullable = False)
    def __repr__(self):
        return("\n\naddress id: " + "%s" +
                "\n employee".ljust(14) + "%s" +
                "\n street".ljust(14) + "%s" +
                "\n zip".ljust(14) + "%s") % (self.id, self.employee.name, self.street, self.zip)

## insert at end of file ## 
engine = create_engine('sqlite:///restaurant.db')
Base.metadata.create_all(engine)