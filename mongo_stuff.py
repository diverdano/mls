#!/usr/bin/env python

import pymongo

## =================================
## === i/o functions for testing ===
## =================================

def setDB(db=1):        ## move thess to cfg file
    db = int(db)
    if db == 1 :
        db_obj = {'host':'mongodb://192.168.0.24','port':27017}
        return pymongo.MongoClient(db_obj['host'], port=db_obj['port']).rplace
    elif db == 2:
        db_obj = {'host':'mongodb://192.168.0.24','port':27017}
        return pymongo.MongoClient(db_obj['host'], port=db_obj['port']).rplace-stage
    elif db == 3:
        db_obj = {'host':'mongodb://localhost','port':27017}
        return pymongo.MongoClient(db_obj['host'], port=db_obj['port']).testDB
    elif db == 4:
        db_obj = {'host':'mongodb://localhost','port':27017}
        return pymongo.MongoClient(db_obj['host'], port=db_obj['port'])
    print(util.logger('Status') + str(db_obj))
    return pymongo.MongoClient(db_obj['host'], port=db_obj['port']).model_perf

## ===============
## === classes ===
## ===============

class RPLACE(object):
    '''wrapper object for data collections'''
    def __init__(self, db=1):
        DBObject        = setDB(db)
        self.users      = Users(DBObject)
        self.users.getDict()
        self.objectives = Objectives(DBObject)
        self.objectives.getDict()


## ====================
## === data objects ===
## ====================

class DataObject(object):
    sort_key='name'
    def __init__(self, DBObject):
        self.collection = DBObject.users
    def create(self, document)      : self.collection.insert(document)
    def read(self, query={}, key=sort_key) : return [item for item in self.collection.find(query).sort(key,1)]
    def update(self, id, document)  : self.collection.update({'_id': bson.ObjectId(id)},{'$set':document})
    def delete(self, id)            : self.collection.remove({'_id': bson.ObjectId(id)})
    def cursor(self, query={})      : return self.collection.find(query) # for iterating downstream
    def count(self, query={})       : return self.collection.find(query).count()
    def lookupId(self, id)          : return [item for item in self.collection.find({'_id': bson.ObjectId(id)})]
    def bulkInsert(self, records):
        if records == []:
            print(util.logger('Status') + 'records sent to bulk insert call: ' + format(len(records),',d'))
            return
        bulk = self.collection.initialize_unordered_bulk_op()
        for item in records:
            bulk.insert(item)
        return bulk.execute()
    def aggregate(
            self,
            m={'$match':{}},
            g={'$group':{'_id':'$sort_key','count':{'$sum':1}}},
            s={'$sort':{'_id':1}}
        ) :
#    def aggregate(self, m={'$match':{}}, g={'$group':{'_id':'$sort_key','count':{'$sum':1}}}) :
        return self.collection.aggregate(m,g,s)
    def getDict(self):
        self.dict = {}
        for item in self.read():
            name = item.get('name','n/a')
            item.pop('name')
            item.pop('_id')
            self.dict[name] = item

class Users(DataObject):
    def __init__(self, DBObject) : self.collection = DBObject.users

class Objectives(DataObject):
    def __init__(self, DBObject) : self.collection = DBObject.objectives
