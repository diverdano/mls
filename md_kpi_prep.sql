/*
drop view public.md_ytd_process_day_lookup;
drop view public.md_temp_ytd;
drop view public.md_mtd_process_day_lookup;
drop view public.md_temp_mtd;
drop view public.md_fund_vw2;
drop view public.md_fund_vw1;

GRANT ALL on public.md_ytd_process_day_lookup to mdetlet, drigoli;
GRANT ALL on public.md_temp_ytd to mdetlet, drigoli;
GRANT ALL on public.md_mtd_process_day_lookup to mdetlet, drigoli;
GRANT ALL on public.md_temp_mtd to mdetlet, drigoli;
GRANT ALL on public.md_fund_vw2 to mdetlet, drigoli;
GRANT ALL on public.md_fund_vw1 to mdetlet, drigoli;

*/

-- Create the inital data extract from vw_opportunity_source
create view public.md_fund_vw1 as
select id_loan_app,
       term,
       housing_type,
       stage,
       status,
       date_funded,
       valid_dm_lead,
       utm_source,
       biz_source,
       da_campaign,
       grade,
       dti_post_loan,
       dti_pre_loan,
       orig_fee,
       principal_remaining,
       principal,
       fico,
       apr,
       annual_income,
       case when utm_source != '' then utm_source
            when biz_source != '' then biz_source
            when da_campaign != '' then da_campaign
            when utm_source = '' and biz_source = '' and da_campaign = '' then 'Organic' end as src1
from public.vw_opportunity_source
where date_funded >= '01-JAN-2015' ;


-- Apply rules to define our Final Channel
create view public.md_fund_vw2 as
select *,
       case when valid_dm_lead = 'true' then 'Direct Mail'
            when upper(substring(src1,1,3)) = 'DM-' then 'Direct Mail'
            when upper(substring(src1,1,3)) = 'TUX' then 'Direct Mail'
            when upper(substring(src1,1,12)) = 'GOCIRCLEBACK' then 'Direct Mail'
            when upper(substring(src1,1,7)) = 'ORGANIC' then 'Organic'
            when upper(substring(src1,1,11)) = 'LENDINGTREE' then 'LendingTree'
            when upper(substring(src1,1,6)) = 'GOOGLE' then 'SEM'
            when upper(substring(src1,1,4)) = 'BING' then 'SEM'         
            when upper(substring(src1,1,5)) = 'YAHOO' then 'SEM'           
            when upper(substring(src1,1,6)) = 'TURING' then 'Sprinklr' else 'Other' end as Source,
       extract(year from date_funded) as Fund_Year,
       extract(month from date_funded) as Fund_Mth,
       extract(quarter from date_funded) as Fund_Qtr,
       ( select count( distinct date_funded ) from public.md_fund_vw1 where extract(year from date_funded) = extract(year from sysdate) ) as process_day_max_ytd,
       ( select count( distinct date_funded ) from public.md_fund_vw1 where extract(month from date_funded) = extract(month from sysdate) and extract(year from date_funded) = extract(year from sysdate) ) as process_day_max_mtd,
       LAST_DAY(ADD_MONTHS(date_funded,-1))+1 as FirstDayOfTheMonth,
       LAST_DAY(ADD_MONTHS(date_funded,(-1*(extract(month from date_funded)))))+1 as FirstDayOfTheYear
from public.md_fund_vw1;


-- Lookup for number of processing days MTD
create view public.md_temp_mtd as select distinct date_funded, FirstDayOfTheMonth from public.md_fund_vw2 ;
create view public.md_mtd_process_day_lookup as
select date_funded, FirstDayOfTheMonth, count(*) as processing_day
from ( select a.date_funded,
              a.FirstDayOfTheMonth
       from public.md_temp_mtd a, public.md_temp_mtd b
       where b.date_funded between a.FirstDayOfTheMonth and a.date_funded order by a.date_funded, b.date_funded )
group by 1,2
order by 1;


create view public.md_temp_ytd as select distinct date_funded, FirstDayOfTheYear from public.md_fund_vw2 ;
create view public.md_ytd_process_day_lookup as
select date_funded, FirstDayOfTheYear, count(*) as processing_day
from ( select a.date_funded,
              a.FirstDayOfTheYear
       from public.md_temp_ytd a, public.md_temp_ytd b
       where b.date_funded between a.FirstDayOfTheYear and a.date_funded order by a.date_funded, b.date_funded )
group by 1,2
order by 1;


-- Append the mtd_flag to the data
create view public.md_fund_vw3 as
select a.*, 
       case when b.processing_day < a.process_day_max_mtd then 1 else 0 end as mtd_processing_flag,
       case when c.processing_day < a.process_day_max_ytd then 1 else 0 end as ytd_processing_flag
from public.md_fund_vw2 a left join public.md_mtd_process_day_lookup b on a.date_funded = b.date_funded
                          left join public.md_ytd_process_day_lookup c on a.date_funded = c.date_funded ;


-- Create the summary report
create view public.md_fund_sum as
select date_funded as Funding_Date,
       Fund_Mth,
       Fund_Year,
       Fund_Qtr,
       Source as Final_Channel,
       Grade as CBL_Grade,
       substring(Grade,1,1) as Risk_Grade,
       Status as CBL_Loan_Status,
       Housing_Type,
       term as Loan_Term,
       1 as Funded,
       sum(principal) as Loan_Principal_Amount,
       sum(principal_Remaining) as Principal_Remaining_Total,
       
       sum(case when mtd_processing_flag = 1 then 1 else 0 end) as MTD_Funded,
       sum(case when mtd_processing_flag = 1 then principal else 0 end) as MTD_Loan_Principal_Amount,

       sum(case when ytd_processing_flag = 1 then 1 else 0 end) as YTD_Funded,
       sum(case when ytd_processing_flag = 1 then principal else 0 end) as YTD_Loan_Principal_Amount,
       
       sum(case when Status = 'Current' then Principal_Remaining else 0 end) as Principal_Remaining_Current,
       sum(case when Status = 'Late 1+' then Principal_Remaining else 0 end) as Principal_Remaining_1to29,
       sum(case when Status = 'Late 30+' then Principal_Remaining else 0 end) as Principal_Remaining_30to59,
       sum(case when Status = 'Late 60+' then Principal_Remaining else 0 end) as Principal_Remaining_60to89,
       sum(case when Status = 'Late 90+' then Principal_Remaining else 0 end) as Principal_Remaining_90Plus,
       sum(case when Status = 'Charged Off' then Principal_Remaining else 0 end) as Principal_Remaining_ChargeOff,     
       
       sum(orig_fee) as Orination_Fee_Value,
       sum(dti_pre_loan) as Pre_Loan_DTI_Ratio,
       sum(dti_post_loan) as Post_Loan_DTI_Ratio,
       sum(FICO) as FICO,
       
       sum(case when Status = 'Current' then 1 else 0 end) as Current,
       sum(case when Status = 'Late 1+' then 1 else 0 end) as Late1to29,
       sum(case when Status = 'Late 30+' then 1 else 0 end) as Late30to59,                        
       sum(case when Status = 'Late 60+' then 1 else 0 end) as Late60to89,
       sum(case when Status = 'Late 90+' then 1 else 0 end) as Late90Plus,  
       sum(case when Status = 'Charged Off' then 1 else 0 end) as ChargedOff,
       sum(case when Status = 'Paid in Full' then 1 else 0 end) as PaidinFull 
from public.md_fund_vw3
group by 1,2,3,4,5,6,7,8,9,10;


-- Export the results to csv
wbexport -file=sql_dvr_output.csv -type=text -delimiter=,;
select * from public.md_fund_sum


