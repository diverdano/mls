drop view if exists public.vw_opportunity;

create or replace view public.vw_opportunity as
select
loan_application_id__c as id_loan_app
, loanid__c as id_loan
, affiliate_lead_id__c as id_affiliate
, lead_id__c as id_lead
, affiliate__c as affiliate
, nvl(b.name, '') as biz_adgroup
, nvl(c.name, '') as biz_adword
, nvl(d.name, '') as biz_term
, nvl(e.name, '') as biz_source
, bizible__device__c as biz_device
, utm_campaign__c as utm_campaign
, utm_content__c as utm_content
, utm_medium__c as utm_medium
, utm_source__c as utm_source
, utm_term__c as utm_term
, stagename as stage
, cbl_loan_status__c as status
, (case when step_1_date_time__c > '' then cast(step_1_date_time__c as date) end) as date_step1
, (case when step_2_date_time__c > '' then cast(step_2_date_time__c as date) end) as date_step2
, (case when step_3_date_time__c > '' then cast(step_3_date_time__c as date) end) as date_step3
, (case when step_4_date_time__c > '' then cast(step_4_date_time__c as date) end) as date_step4
, (case when step_5_date_time__c > '' then cast(step_5_date_time__c as date) end) as date_step5
, (case when canceled_date_time__c > '' then cast(canceled_date_time__c as date) end) as date_canceled
, (case when completed_date_time__c > '' then cast(completed_date_time__c as date) end) as date_completed
, (case when declined_review_date_time__c > '' then cast(declined_review_date_time__c as date) end) as date_declined
, (case when funded_date_time__c > '' then cast(funded_date_time__c as date) end) as date_funded
, (case when payoff_date__c > '' then cast(payoff_date__c as date) end) as date_payoff
, (case when ssn_verify_date__c > '' then cast(ssn_verify_date__c as date) end) as date_ssn_verify
, canceled_reason__c as reason_canceled
, declined_rev_reason__c as reason_declined
, housing_type__c as housing_type
, annual_income__c as annual_income
, cbl_grade__c as grade
, dti_ratio__c as dti_pre_loan
, post_loan_dti_ratio__c as dti_post_loan
, fico__c as fico
, housing_payment__c as housing_payment
, fraud_review_bucket__c as fraud_review_bucket
, fraud_score__c as TU_fraud_score
, first_contact_amount__c as req_amount
, first_contact_purpose__c as req_purpose
, current_payment_amount__c as payment_amount
, finance_charges__c as finance_charges
, loan_apr__c as apr
, loan_interest__c as interest
, loan_principal_amount__c as principal
, loan_term__c as term
, origination_fee_value__c as orig_fee
, principal_remaining__c as principal_remaining
, cbl_payment_type__c as cbl_payment_type
, payment_plan__c as payment_plan
, total_amount_paid__c as total_paid
, total_of_monthly_payments__c as total_mo_payments
, borrower_email__c as email
, do_not_call__c as do_not_call
from cbl_landing.opportunity a
left join cbl_landing.bizible__Ad_Group__c b
on a.bizible__ad_group__c = b.id
left join cbl_landing.bizible__adwords_campaign__c c
on a.bizible__adwords_campaign__c = c.id
left join cbl_landing.bizible__keyword__c d
on a.bizible__keyword__c = d.id
left join cbl_landing.bizible__websource__c e
on a.bizible__websource__c = e.id;

grant select on public.vw_opportunity to drigoli;
